import React from 'react'
import { NavLink } from 'react-router-dom'

export default function MyStationsPage () {
  return (
    <div>
      <h1>WELCOME TO the my stations page</h1>
      <p>Here you can see all the stations you have played and created.</p>
    </div>
  )
}
