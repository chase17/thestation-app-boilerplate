import React, { useState } from 'react'
import { addStation, createStation } from '../Backend/StationsDB'
import { Link, useHistory } from 'react-router-dom'

export default function CreatePage () {
  const [station, setStation] = useState({
    stationID: '',
    title: '',
    category: '',
    description: ''
  })

  const history = useHistory()

  function handleTitleChange (event) {
    const titleValue = event.target.value

    setStation(curActivity => {
      return {
        ...curActivity,
        title: titleValue
      }
    })
    console.log('title is' + titleValue + ' OR ' + station.title)
  }
  function handleCategoryChange (event) {
    const categoryValue = event.target.value

    setStation(curActivity => {
      return {
        ...curActivity,
        category: categoryValue
      }
    })
    console.log('category is' + categoryValue)
  }
  function handleDescriptionChange (event) {
    const descriptionValue = event.target.value

    setStation(curActivity => {
      return {
        ...curActivity,
        description: descriptionValue
      }
    })
    console.log('description is' + descriptionValue)
  }

  function startCreateStation (event) {
    let stationString =
      'station' +
      Math.random()
        .toString(36)
        .substring(7)
    let stationNum = Math.floor(1000 + Math.random() * 9000)

    const newID = stationString + stationNum
    console.log(newID)

    setStation(curActivity => {
      return {
        ...curActivity,
        stationID: newID
      }
    })
    createStation(station, newID)

    history.push('/create/' + newID)
    event.preventDefault()
  }

  return (
    <div className='CreatePage'>
      <h1>Create a Station</h1>
      <p>
        Create an interactive group experience with content, trivia, games, and
        activities.
      </p>
      <form className='CreateStation'>
        <label>
          title
          <input type='text' name='title' onChange={handleTitleChange} />
        </label>
        <label>
          category
          <input type='text' name='category' onChange={handleCategoryChange} />
        </label>
        <label>
          description
          <input
            type='text'
            name='description'
            onChange={handleDescriptionChange}
          />
        </label>
        <button type='submit' onClick={startCreateStation}>
          START CREATING!
        </button>
      </form>
    </div>
  )
}
