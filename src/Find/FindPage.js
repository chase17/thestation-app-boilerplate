import React from 'react'
import { NavLink } from 'react-router-dom'

export default function FindPage () {
  return (
    <div>
      <h1>Welcome To The find page</h1>
      <p>Here you can find a station</p>
    </div>
  )
}
