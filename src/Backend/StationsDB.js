import firebase from '../firebase'
import Auth from '../Auth'

export async function addStation (station) {
  console.log('add Station to firestore')
  const userName = Auth.getUserName()
  const profileImg = Auth.getProfileImg()

  const newStation = firebase
    .firestore()
    .collection('Stations')
    .add({
      stationID: '',
      creatorID: userName,
      creatorImage: profileImg,
      title: station.title,
      category: station.category,
      description: station.description,
      crewSize: station.crewSize,
      time: station.time,
      share: station.share
    })
    .then(function (docRef) {
      console.log('Document written with ID: ', docRef.id)
      setStationID(docRef.id)
    })
    .catch(function (error) {
      console.error('Error adding document: ', error)
    })

  function setStationID (id) {
    const newStationID = firebase
      .firestore()
      .collection('Stations')
      .doc(id)
      .update({
        stationID: id
      })
  }
}

export async function createStation (station, newID) {
  console.log('add Station to firestore')
  const userName = Auth.getUserName()
  const profileImg = Auth.getProfileImg()

  const newStation = firebase
    .firestore()
    .collection('Stations')
    .doc(newID)
    .set({
      stationID: newID,
      creatorID: userName,
      creatorImage: profileImg,
      title: station.title,
      category: station.category,
      description: station.description
    })
    .then(function (docRef) {
      //console.log('Document written with ID: ', docRef.id)
    })
    .catch(function (error) {
      console.error('Error adding document: ', error)
    })

  // function setStationID (id) {
  //   const newStationID = firebase
  //     .firestore()
  //     .collection('Stations')
  //     .doc(id)
  //     .update({
  //       stationID: id
  //     })
  // }
}

export async function addImgToStation (imgContent, stationID) {
  const newStation = firebase
    .firestore()
    .collection('Stations')
    .doc(stationID)
    .update({
      sequence: firebase.firestore.FieldValue.arrayUnion(imgContent)
    })
    .then(function (docRef) {
      //console.log('Document written with ID: ', docRef.id)
    })
    .catch(function (error) {
      console.error('Error adding document: ', error)
    })
}

export async function addVidToStation (vidName, statinoID) {}

export async function getStationData (stationID) {
  let docRef = firebase
    .firestore()
    .collection('Stations')
    .doc(stationID)

  let docData

  const stationData = docRef
    .get()
    .then(function (doc) {
      if (doc.exists) {
        console.log('Document data:', doc.data())
        docData = doc.data()
        return docData
      } else {
        // doc.data() will be undefined in this case
        console.log('No such document!')
        return docData
      }
    })
    .catch(function (error) {
      console.log('Error getting document:', error)
    })
  return stationData
}

export async function addUserToActivity (activityID) {
  const userName = Auth.getUserName()
  const profileImg = Auth.getProfileImg()

  console.log('put user in firestore')
  const activities = firebase
    .firestore()
    .collection('Activities')
    .doc(activityID)
    .collection('MembersGoing')
    .doc(userName)
    .set({
      name: userName,
      profileImg: profileImg
    })
  return activities
}

export async function deleteUserFromActivity (activityID) {
  const userName = Auth.getUserName()

  const deleteUser = firebase
    .firestore()
    .collection('Activities')
    .doc(activityID)
    .collection('MembersGoing')
    .doc(userName)
    .delete()
}

// db.collection("cities").where("capital", "==", true)
//     .get()
//     .then(function(querySnapshot) {
//         querySnapshot.forEach(function(doc) {
//             // doc.data() is never undefined for query doc snapshots
//             console.log(doc.id, " => ", doc.data());
//         });
//     })
//     .catch(function(error) {
//         console.log("Error getting documents: ", error);
//     });

// useEffect(()=>{
//   firebase.firestore().collection('Quizes').doc(props.match.params.id).get().then(doc => {
//       setDoc(doc);
//       setQuestions (doc.data().Questions);
//   })
// },[props.match.params.id])
// const addQuestion= () => {
//   setQuestions([...Questions, {
//       Question: '', A: '', B: '', C: '', D: '', Aans: false, Bans: false, Cans: false, Dans: false
//   } ])
// }
