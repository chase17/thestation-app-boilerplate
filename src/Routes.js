import React from 'react'
import { Route } from 'react-router-dom'
import AuthRoute from './AuthRoute'
import HomePage from './Home/HomePage'
import ProfilePage from './Profile/ProfilePage'
import CreatePage from './Create/CreatePage'
import CreateStationPage from './Create/CreateStation/CreateStationPage'
import FindPage from './Find/FindPage'
import JoinPage from './Join/JoinPage'
import MyStationsPage from './MyStations/MyStationsPage'

export default function Routes () {
  return (
    <div className='routes'>
      <Route exact path='/' component={HomePage} />
      <Route exact path='/profile' component={ProfilePage} />
      <Route exact path='/create' component={CreatePage} />
      <Route exact path='/create/:stationID' component={CreateStationPage} />
      <Route exact path='/find' component={FindPage} />
      <Route path='/join' component={JoinPage} />
      <Route exact path='/myStations' component={MyStationsPage} />
    </div>
  )
}
