import React, { useState, useEffect } from 'react'
import { NavLink, useHistory } from 'react-router-dom'
import './HomePage.scss'

export default function HomePage () {
  const history = useHistory()

  return (
    <div className='HomePage'>
      <p>
        The Station is an educational platform that gives teachers and students
        the tools they need to create interactive group experiences.
      </p>

      <h4> (TODO) This is where the home page design goes. </h4>
    </div>
  )
}
